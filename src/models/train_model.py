import os
print("TM pwd:", os.getcwd())
from mlflow.models.signature import infer_signature
from catboost import CatBoostClassifier
import mlflow

import sys
print("TM path: ", sys.path)
sys.path.append('.')
from src.data.load_dataset import load_dataset_eth_contest


def train_cb_default():
    mlflow.set_tracking_uri("http://127.0.0.1:5000")
    mlflow.set_experiment("catboost default")
    mlflow.sklearn.autolog()
    with mlflow.start_run():
        df_train_X, df_train_y, df_test_X, df_test_y = load_dataset_eth_contest()
        cb_model = CatBoostClassifier(iterations=100, max_depth=3, random_state=42, text_features=["message__all"])
        cb_model.fit(df_train_X, df_train_y)

        predictions = cb_model.predict(df_test_X)
        signature = infer_signature(df_train_X, predictions)
        mlflow.sklearn.log_model(cb_model, "tst_model", signature=signature)

        autolog_run = mlflow.last_active_run()


if __name__ == "__main__":
    train_cb_default()
