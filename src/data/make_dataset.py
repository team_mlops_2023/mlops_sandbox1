"""
Contains calls of dataset making functions
"""
# -*- coding: utf-8 -*-
import logging
from pathlib import Path
import click
from dotenv import find_dotenv, load_dotenv


def get_tweets_df_for_ticker(
    file_path: str,
    first_quote_date_str: str = None,
    last_quote_date_str: str = None,  # Like "2023-02-02" or None for using all data
    verbose: bool = True
) -> pd.DataFrame:
    print(f"Getting data for {file_path=} and {first_quote_date_str=} and {last_quote_date_str=}")
    df = get_company_data_v2(file_path)
    if verbose:
        print(f"\nDBG Head:\n", df.head())
        print(f"\nDBG Tail:\n", df.tail())

    print("Processing datetime column")
    #df['datetime'] = df['datetime'].astype('datetime64[ns]')
    df['datetime'] = pd.to_datetime(df['datetime'], errors='raise')
    df['Date'] = pd.to_datetime([d.date() for d in df['datetime']], errors='raise')
    # df['Time'] = [d.time() for d in df['datetime']]  # Cannot convert to datetime type
    df['Weekday'] = [d.date().weekday() for d in df['Date']]

    # Filter by dates, if required
    print(f"Filtering by dates")
    shape_before = df.shape
    if first_quote_date_str is not None:
        first_quote_date =  dt.datetime.strptime(first_quote_date_str, "%Y-%m-%d").astimezone(dt.timezone.utc)  # tz_localize('utc')
        df = df[df.datetime >= first_quote_date]
    if last_quote_date_str is not None:
        last_quote_date =  dt.datetime.strptime(last_quote_date_str, "%Y-%m-%d").astimezone(dt.timezone.utc)  # tz_localize('utc')
        df = df[df.datetime <= last_quote_date]
    print(f".. shape before: {shape_before}, shape after: {df.shape}")
    if verbose:
        print(f"\nDBG Head:\n", df.head())
        print(f"\nDBG Tail\n", df.tail())

    return df

@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def main(input_filepath, output_filepath):   # pylint: disable=unused-argument
    """Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("making final data set from raw data")

    # TBD: fill with logic
    raise NotImplementedError()


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()  # pylint: disable=no-value-for-parameter
