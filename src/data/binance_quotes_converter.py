"""
Functions for working with binance quotes
"""
import json
import click
import pandas as pd


# Constants for columns
COL__TIMESTAMP_UTC = "timestamp_utc"
COL__DATETIME_UTC = "datetime_utc"
COL__OPEN = "open"
COL__HIGH = "high"
COL__LOW = "low"
COL__CLOSE = "close"
COL__VOLUME = "volume"


def _convert_quotes_from_timestamp_to_datetime(src_json_data: dict) -> pd.DataFrame:
    """
    Converts json data (Binance format) with timestamps into pandas DataFrame with datetime format.
    Args:
        src_json_data: Loaded json object.
    Returns:
        Pandas DataFrame object.
    """
    # Convert json data to DataFrame
    df_tmp = pd.DataFrame(
        src_json_data,
        columns=[
            COL__TIMESTAMP_UTC,
            COL__OPEN,
            COL__HIGH,
            COL__LOW,
            COL__CLOSE,
            COL__VOLUME,
        ],
    )

    # Convert timestamp column to datetime, preserving it's first position
    df_tmp.insert(
        0, COL__DATETIME_UTC, pd.to_datetime(df_tmp.pop(COL__TIMESTAMP_UTC), unit="ms")
    )

    return df_tmp


@click.command()
@click.argument("src_json_file_path", type=click.Path(exists=True))
@click.argument("dst_csv_file_path", type=click.Path(exists=False))
def binance_quotes_converter(src_json_file_path: str, dst_csv_file_path: str) -> None:
    """
    Command-line utility for converting a Binance json file with quotes (timestamps are used there) to
    pandas csv (or csv.gz) file with datetimes in UTC.
    Args:
        src_json_file_path: Path to source json file with Binance quotes.
        dst_csv_file_path: Path to output file (*.csv, *.csv.gz, etc).
    Returns:
        None
    """
    assert isinstance(src_json_file_path, str)  # Check if `click.Path` derives from str

    # Load the JSON file
    with open(src_json_file_path, encoding="utf-8") as f:
        json_data = json.load(f)

    df_res = _convert_quotes_from_timestamp_to_datetime(json_data)

    # Write dataframe to output file, optionally with compression (depends on file extension)
    df_res.to_csv(dst_csv_file_path, index=False, compression="infer")


if __name__ == "__main__":
    binance_quotes_converter()  # pylint: disable=no-value-for-parameter
