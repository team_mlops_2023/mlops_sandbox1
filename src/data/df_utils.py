"""
"DataFrame utils" - utilities for ML-related tasks, based on pandas DataFrames
"""
from __future__ import annotations

import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, roc_auc_score


################################################################################
# Part 1: helper functions (could be used standalone)
################################################################################


def get_array_from_any_1d_seq(
    y: np.ndarray | pd.Series | pd.DataFrame | list | tuple,
) -> np.ndarray:
    """
    Ensure that input is one of the 1-dimensional sequence types, and convert it to numpy array.
    Type of data is not checked (could be even non-numeric).
    Args:
        y: input data as 1-d sequence
    Returns:
        data, converted to 1-d numpy array
    """
    # Check input type and do conversions
    if isinstance(y, np.ndarray):
        res = np.squeeze(y)  # Example: shape(1, 3) -> (3,),  shape(3, 1) -> (3,)
    elif isinstance(y, pd.Series):
        res = y.values
    elif isinstance(y, pd.DataFrame):
        assert (
            len(y.columns) == 1
        ), f"Only 1-column dataframes are expected. Cur shape: {y.shape}"
        res = y.iloc[:, 0].values
    elif isinstance(y, (list, tuple)):
        res = np.array(y)
    else:
        assert False, f"Unexpected type: {type(y)}"

    # Final check of output
    assert isinstance(res, np.ndarray)
    assert res.ndim == 1
    return res


def calc_class_balance(y: np.ndarray | pd.Series | pd.DataFrame | list | tuple) -> str:
    """
    Calculate balance of classes. Currently, supports 2-class and 3-class labels.
    Args:
        y: 1-dimensional sequence of integers
    Returns:
        String with balance values, like: '60.0%:40.0%'
    """
    y_arr = get_array_from_any_1d_seq(y)
    cnts = pd.Series(y_arr).value_counts()
    if len(cnts) == 2:
        bal0 = cnts[0] / cnts.sum()
        bal1 = cnts[1] / cnts.sum()
        assert np.isclose(bal0 + bal1, 1.0)
        return f"{bal0:.1%}:{bal1:.1%}"

    if len(cnts) == 3:
        bal0 = cnts[-1] / cnts.sum()
        bal1 = cnts[0] / cnts.sum()
        bal2 = cnts[1] / cnts.sum()
        assert np.isclose(bal0 + bal1 + bal2, 1.0)
        return f"{bal0:.1%}:{bal1:.1%}:{bal2:.1%}"

    assert False, f"Unexpected number of classes: {cnts}"


def calc_hash_for_seq(
    y: np.ndarray | pd.Series | pd.DataFrame | list | tuple, hash_len: int = 6
):
    """
    Calculate hash of input data.
    Example: np.array([1, 2, 3]) -> '1bcceb'
    Args:
        y: Input 1-dim sequence of values.
        hash_len: Length of output hash string

    Returns:
        Hash value of data, as hex string.
    """
    y_arr = get_array_from_any_1d_seq(y)
    h = hash(tuple(y_arr))
    return f"{h:08x}"[-hash_len:]


def get_statistics_for_x_y__legend() -> str:
    """
    Generate a legend string, corresponding to the `get_statistics_for_x_y` method output.
    Returns:
        Legend string.
    """
    return "X_shape; y_shape; hash;sum;balance"


def get_statistics_for_x_y(
    X: np.ndarray | pd.DataFrame | pd.Series,
    y: np.ndarray | pd.DataFrame | pd.Series | list | tuple | None,
) -> str:
    """
    Calculate misc metrics for X set (shape) and optional y set (shape, hash, sum, balance).
    Note: equal hashes typically mean binary equality, equal sums could mean that rows are the same, but were shuffled.
    Example: '(5, 3); (5,); 1a1c92;3;40.0%:60.0%'
    See Also: `get_statistics_for_x_y__legend`
    Args:
        X: Dataframe (or Series) with features (or just full dataset if labels are absent for now)
        y: (optional) 1d-sequence with labels.
    Returns:
        statistics string
    """
    if y is None:
        y_stats = "; ;;"  # Preserve structure for possible results parsing
    else:
        y_arr = get_array_from_any_1d_seq(y)
        assert len(X) == len(y), f"Inconsistent shapes: {X.shape=} vs {y_arr.shape=}"
        y_stats = f"{y_arr.shape}; {calc_hash_for_seq(y_arr)};{sum(y_arr)};{calc_class_balance(y_arr)}"

    stats = f"{X.shape}; {y_stats}"
    return stats


# pylint: disable=R0914  # Ignore too many local variables check
def calc_partial_test_scores(
    probas_test: np.ndarray | pd.DataFrame | pd.Series | list | tuple,
    y_true_test: np.ndarray | pd.DataFrame | pd.Series | list | tuple,
    dates_test: np.ndarray | pd.DataFrame | pd.Series | list | tuple,
    n_parts: int = 5,
    verbose: bool = True,
):
    """
    Measures accuracy and roc auc scores on multiple pieces of the test set.
    Typical usage:
        probas = est.predict_proba(X_test)[:, 1]
        df = pd.DataFrame(calc_partial_test_scores(probas, y_true, X['datetime']))
        df.plot(style="o--", grid=True)
    Args:
        probas_test: 1d sequence of floats
        y_true_test: 1d sequence of integers (0, 1)
        dates_test: 1d sequence of datetime strings or values
        n_parts: number of parts to split
        verbose: if True, scores will be printed

    Returns:
        Dictionary with fields 'datetime', 'acc', 'auc'
    """
    arr_probas = get_array_from_any_1d_seq(probas_test[:, 1])
    arr_y_true = get_array_from_any_1d_seq(y_true_test)
    arr_dates = get_array_from_any_1d_seq(dates_test)
    assert len(arr_probas) == len(arr_y_true) == len(arr_dates)

    indices = list(range(len(arr_probas)))

    scores_acc = []
    scores_auc = []
    datetimes = []
    for i, partition in enumerate(np.array_split(indices, n_parts)):
        arr_dates_part = arr_dates[partition]
        arr_probas_part = arr_probas[partition]
        arr_y_true_part = arr_y_true[partition]

        datetimes.append(arr_dates_part[0])
        scores_acc.append(
            accuracy_score(arr_y_true_part, np.where(arr_probas_part >= 0.5, 1, 0))
        )
        scores_auc.append(roc_auc_score(arr_y_true_part, arr_probas_part))
        if verbose:
            print(
                f"{i} {datetimes[-1]} len={len(arr_y_true_part)} acc={scores_acc[-1]:.3f} auc={scores_auc[-1]:.4f}"
            )

    return {"datetime": datetimes, "acc": scores_acc, "auc": scores_auc}


def drop_some_rows_inplace(
    df: pd.DataFrame,
    linked_df: pd.DataFrame = None,
    n_final_rows: int = 1000,
    seed: int = None,
    verbose: bool = True,
) -> None:
    """
    Randomly drop extra rows (inplace) without shuffling the dataset.
    Could be used to implement "fast-check" mode, etc.
    Args:
        df: Main dataframe to be decimated.
        linked_df: Optional dataset, that should be synced with the main df. Example: labels.
        n_final_rows: Number of rows to preserve.
        seed: Random state that is used for choosing what rows to drop. None will use some internal entropy.
        verbose: if True, operation summary will be printed.
    Returns: None
    """
    old_len = len(df)
    if linked_df is not None:
        assert list(linked_df.index) == list(df.index)

    n_rows_to_remove = len(df) - n_final_rows
    if n_rows_to_remove > 0:
        rng = np.random.default_rng(
            seed=seed
        )  # Use own generator to avoid potential np.random interference.
        drop_indices = rng.choice(df.index, n_rows_to_remove, replace=False)
        df.drop(drop_indices, inplace=True)
        if linked_df is not None:
            linked_df.drop(drop_indices, inplace=True)

    if verbose:
        new_len = len(df)
        print(
            f"Some random rows dropped (without shuffling). "
            f"Before: {old_len}, after: {new_len}, "
            f"diff: {new_len - old_len} ({new_len / old_len if old_len > 0 else 0.0:.2%} left)"
        )


################################################################################
# Part 2: DataFrameWithHistory class
################################################################################


class DataFrameWithHistory:
    """
    Simple wrapper for pandas DataFrame, that holds text comments of all data modifications ("transactions").
    Usage example:
        df_tmp = pd.DataFrame({'x': [1, 2, np.nan, 3], 'y': [0, 1, 0, 0]})
        dfh_demo = DataFrameWithHistory(df_tmp, col_label='y')
        dfh_demo.modify_dataset(dfh_demo.df.dropna(), "NA values dropped")
        print(dfh_demo.history)

    Expected example output:
        X_shape; y_shape; hash;sum;balance; comment
        (4, 1); (4,); 986649;1;75.0%:25.0%; Initial creation
        (3, 1); (3,); 907f06;1;66.7%:33.3%; NA values dropped
    """

    def __init__(
        self,
        df: pd.DataFrame,
        col_label: str | None = None,
        initial_comment: str = "Initial creation",
    ):
        """
        Initial object creation.
        Args:
            df: source dataset (should be pandas DataFrame object)
            col_label: (optional) name of column with labels. If set, the `df_X` and `df_y` properties could be used.
            initial_comment: user description of initial data state
        """
        self._df: pd.DataFrame | None = None
        self._col_label: str | None = None
        self._transaction_history: list[
            str
        ] = []  # For each transaction: new statistics and comment

        self.modify_dataset(df_new=df, new_col_label=col_label, comment=initial_comment)

    def _append_transaction_info(self, transaction_comment: str):
        data_stats = self.current_data_stats
        self._transaction_history.append(f"{data_stats}; {transaction_comment}")

    @property
    def current_data_stats(self):
        """
        Returns: Statistics: shapes, optionally, hashes, etc. for the stored data.
        Example: "(4, 1); (4,); 986649;1;75.0%:25.0%"
        """
        if self.col_label is None:
            info = get_statistics_for_x_y(self.df, None)
        else:
            info = get_statistics_for_x_y(self.X, self.y)
        return info

    @property
    def col_label(self) -> str | None:
        """
        Returns: Name of column with labels, or None if not set.
        """
        return self._col_label

    @property
    def df(self) -> pd.DataFrame:
        """
        Returns: Copy of stored dataset.
        """
        return self._df.copy()

    @property
    def X(self) -> pd.DataFrame:
        """
        Returns: Dataset without label column. If label column is not set, exception is raised.
        """
        if self._col_label is None:
            raise ValueError(
                "This property should not be used when label column name is not set. See `.df` instead."
            )
        return self._df.loc[:, self._df.columns != self._col_label].copy()

    @property
    def y(self) -> np.ndarray:
        """
        Returns: Label column values (as np.ndarray). If label column is not is set, exception is raised.
        Note: pd.Series is not used to avoid potential issues like: df['y'] = y (order will depend on indices)
        """
        if self._col_label is None:
            raise ValueError(
                "This property should not be used when label column name is not set. See `.df` instead."
            )
        return self._df[self._col_label].to_numpy().copy()

    @property
    def legend(self):
        """
        Returns: Legend for `history` property.
        """
        return get_statistics_for_x_y__legend() + "; comment"

    @property
    def history(self):
        """
        Returns: Transaction history. See also the `legend` property.
        """
        return "\n".join([self.legend] + self._transaction_history)

    def modify_dataset(
        self, df_new: pd.DataFrame, comment: str = "", new_col_label: str | None = ""
    ) -> None:
        """
        Public method for updating data. Column for labels could be modified or unset.
        Args:
            df_new: new data
            new_col_label: (optional) Could be '' (leave as is), None (unset label column), or a non-empty string.
            comment: user comment to describe the data change
        Returns: None
        """
        # Initial checks
        assert isinstance(df_new, pd.DataFrame)
        if new_col_label == "":
            # Do not change anything
            new_col_label = self._col_label
        elif new_col_label is not None:
            assert (
                new_col_label in df_new.columns
            ), f"Column {new_col_label} not found in the specified dataset"

        # Update all fields and register the transaction
        self._df = df_new.copy()
        self._col_label = new_col_label
        self._append_transaction_info(comment)


################################################################################
# Part 3: DataFramePairWithHistory class
################################################################################


class DataFramePairWithHistory:
    """
    Similar to DataFrameWithHistory, but holds two datasets inside - train and test, with history, i.e.
    text comments of all data modifications ("transactions").
    Usage example:
        df_tmp = pd.DataFrame({'x': [1, 2, np.nan, 3], 'y': [0, 1, 0, 0]})
        dfph_demo = DataFramePairWithHistory(df_tmp, df_tmp)
        dfph_demo.modify_dataset(dfph_demo.trn_df.dropna(), dfph_demo.tst_df, "Drop NAs for train")
        print(dfph_demo.history)

    Expected example output:
        'Leg: X_shape; y_shape; hash;sum;balance; comment'
        'Trn: (4, 2); ; ;;; Initial creation'
        'Tst: (4, 2); ; ;;; Initial creation'
        '--'
        'Trn: (3, 2); ; ;;; Drop NAs for train'
        'Tst: (4, 2); ; ;;; Drop NAs for train'
        '--'
    """

    def __init__(
        self,
        df_train_Xy: pd.DataFrame,
        df_test_Xy: pd.DataFrame,
        col_label: str | None = None,
        initial_comment: str = "Initial creation",
    ):
        """
        Initial object creation.

        Args:
            df_train_Xy: source train dataset (should be pandas DataFrame object)
            df_test_Xy: source test dataset (should be pandas DataFrame object)
            col_label: (optional) name of column with labels. If set, the `df_X` and `df_y` properties could be used.
            initial_comment: user description of initial data state
        """
        self._dfh_trn: DataFrameWithHistory = DataFrameWithHistory(
            df_train_Xy, col_label, initial_comment
        )
        self._dfh_tst: DataFrameWithHistory = DataFrameWithHistory(
            df_test_Xy, col_label, initial_comment
        )

    @property
    def current_data_stats(self):
        """
        Returns: Statistics: shapes, optionally, hashes, etc. for the stored data.
        Example:
            'Trn: (4, 1); (4,); 986649;1;75.0%:25.0%; abcd\n'
            'Tst: (4, 1); (4,); 986649;1;75.0%:25.0%; abcd\n'
        """
        info_trn = self._dfh_trn.current_data_stats
        info_tst = self._dfh_tst.current_data_stats
        return f"Trn: {info_trn}\nTst: {info_tst}"

    @property
    def col_label(self) -> str | None:
        """
        Returns: Name of column with labels, or None if not set.
        """
        return self._dfh_trn.col_label  # Use trn object

    @property
    def trn_df(self) -> pd.DataFrame:
        """
        Returns: Copy of stored train dataset (X and y)
        """
        return self._dfh_trn.df

    @property
    def trn_X(self) -> pd.DataFrame:
        """
        Returns: Train dataset without label column. If label column is not set, exception is raised.
        """
        return self._dfh_trn.X

    @property
    def trn_y(self) -> np.ndarray:
        """
        Returns: Train label column values (as np.ndarray). If label column is not is set, exception is raised.
        Note: pd.Series is not used to avoid potential issues like: df['y'] = df_y (order will depend on indices)
        """
        return self._dfh_trn.y

    @property
    def tst_df(self) -> pd.DataFrame:
        """
        Returns: Copy of stored test dataset (X and y)
        """
        return self._dfh_tst.df

    @property
    def tst_X(self) -> pd.DataFrame:
        """
        Returns: Test dataset without label column. If label column is not set, exception is raised.
        """
        return self._dfh_tst.X

    @property
    def tst_y(self) -> np.ndarray:
        """
        Returns: Test label column values (as np.ndarray). If label column is not is set, exception is raised.
        Note: pd.Series is not used to avoid potential issues like: df['y'] = df_y (order will depend on indices)
        """
        return self._dfh_tst.y

    @property
    def legend(self):
        """
        Returns: Legend for `history` property.
        """
        return get_statistics_for_x_y__legend() + "; comment"

    @property
    def history(self):
        """
        Returns: Transaction history. See also the `legend` property.
        """
        res = ["Leg: " + self.legend]
        history_trn = self._dfh_trn.history.split("\n")[1:]  # Skip legend
        history_tst = self._dfh_tst.history.split("\n")[1:]  # Skip legend
        assert len(history_trn) == len(history_tst)
        for trn_h, tst_h in zip(history_trn, history_tst):
            res.append("Trn: " + trn_h)
            res.append("Tst: " + tst_h)
            res.append("--")

        return "\n".join(res)

    def modify_dataset(
        self,
        df_new_train_Xy: pd.DataFrame,
        df_new_test_Xy: pd.DataFrame,
        comment: str = "",
        new_col_label: str | None = "",
    ) -> None:
        """
        Public method for updating data. Column for labels could be modified or unset.
        Args:
            df_new_train_Xy: new data for train
            df_new_test_Xy: new data for test
            new_col_label: (optional) Could be '' (leave as is), None (unset label column), or a non-empty string.
            comment: user comment to describe the data change
        Returns: None
        """
        self._dfh_trn.modify_dataset(df_new_train_Xy, comment, new_col_label)
        self._dfh_tst.modify_dataset(df_new_test_Xy, comment, new_col_label)
