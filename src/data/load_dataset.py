import os
import pandas as pd
print("LDS pwd: ", os.getcwd())

PATH_TO_DATA_FOLDER = './data/raw/2023-05_Dataset_StockTwits_ETH_contest'
FNAME_TEST_X = "2023-06-01T190459__OBCL__ETH-USDT_2021-04_2023-02_fXX_rs42__Test_X.csv.gz"
FNAME_TEST_Y = "2023-06-01T190459__OBCL__ETH-USDT_2021-04_2023-02_fXX_rs42__Test_y.csv.gz"
FNAME_TRAIN_X = "2023-06-01T190459__OBCL__ETH-USDT_2021-04_2023-02_fXX_rs42__Train_X.csv.gz"
FNAME_TRAIN_Y = "2023-06-01T190459__OBCL__ETH-USDT_2021-04_2023-02_fXX_rs42__Train_y.csv.gz"

def load_dataset_eth_contest():
    print(f"Loading data from folder {PATH_TO_DATA_FOLDER} ...")
    df_train_X = pd.read_csv(os.path.join(PATH_TO_DATA_FOLDER, FNAME_TRAIN_X))
    df_train_y = pd.read_csv(os.path.join(PATH_TO_DATA_FOLDER, FNAME_TRAIN_Y))
    df_test_X = pd.read_csv(os.path.join(PATH_TO_DATA_FOLDER, FNAME_TEST_X))
    df_test_y = pd.read_csv(os.path.join(PATH_TO_DATA_FOLDER, FNAME_TEST_Y))

    # Drop unnecessary features
    df_train_X.drop(columns='candle_datetime', inplace=True)
    df_test_X.drop(columns='candle_datetime', inplace=True)

    print("... success")
    return df_train_X, df_train_y, df_test_X, df_test_y
