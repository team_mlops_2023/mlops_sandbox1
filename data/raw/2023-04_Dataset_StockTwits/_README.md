2023-04  

## Dataset description  
The dataset was exported for AMZN 2020-07..2022-07.  
Train set: 2020-07-02..2021-07-01 (~1 year), 184426 records (ideally balanced)  
Test set: 2021-07-02..2022-06-30 (~1 year), 190092 records (ideally balanced)  


Column descriptions (ST stands for "StockTwits", "today" - message date)  

| Column                        | Meaning                                                                                    | Examples                           |
|-------------------------------|--------------------------------------------------------------------------------------------|------------------------------------|
| id                            | record number                                                                              | 449018                             |
| symbol                        | Stock ticker or crypto pair                                                                | "AMZN", "XRP-USDT"                           |
| datetime                      | ST message UTC datetime                                                                    | "2020-07-02 00:31:46+00:00"        |
| user                          | ST user id                                                                                 | 3201586                            |
| message_id                    | ST message id                                                                              | 224775921                          |
| DateStr                       | ST message date ("today")                                                                  | 2020-07-02                         |
| Weekday                       | ST message weekday, 0-Monday, 7-Sunday                                                     | 3                                  |
| price_change_percent          | Change of price (Close/Open) for "today", %                                                | -0.748626373626371                 |
| price_change_percent_prev_day | Change of price (Close/Open) for "yesterday", %                                            | 4.372733865119649                  |
| vol_change_percent            | Change of volume for "today" relative to "yesterday", %                                    | 3.614363136525789                  |
| user_followers                | ST user, number of followers                                                               | 1                                  |
| user_following                | ST user, number of followed                                                                | 1                                  | 
| user_ideas                    | ST user, number of ideas                                                                   | 53                                 | 
| user_like_count               | ST user, number of likes                                                                   | 93                                 |
| len_symbols                   | ST message, number of related symbols <br/>(how many tickers are mentioned in the message) | 1                                  |
| likes_total                   | ST message, number of likes                                                                | 2                                  |
| message                       | ST message text                                                                            | "amzn  2920 at open would be nice" |


Note about potential data leaks:  
Using some columns as feature (even message_id) may cause hidden data leaks!  
One of the possible indicators for this: excellent train set metrics and very bad test set metrics.


Note about label calculation:  
Target label is calculated by strategy "d7_O=d1_O=0.5%=2cls", which corresponds to the following formulas:  
* price_change_ratio = (open_price("today"+7) / open_price("today"+1) - 1.0) * 100%
* if price_change_ratio > 0.5%, then label = 1
* if price_change_ratio < -0.5%, then label = 0
* else: record is dropped


## Change history
2023-04-07: Added "patched" versions of message-level datasets (4 files *_PMid=0.1_*).  
    Patching is applied to 10% of messages (explicit up/down strings are inserted to the message middle). Expected accuracy: >=0.55.  