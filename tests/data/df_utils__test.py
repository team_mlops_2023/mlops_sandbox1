import numpy as np
import pandas as pd

from src.data.df_utils import (
    DataFrameWithHistory,
    DataFramePairWithHistory,
    get_array_from_any_1d_seq,
    calc_class_balance,
    calc_hash_for_seq,
    get_statistics_for_x_y__legend,
    get_statistics_for_x_y,
    drop_some_rows_inplace,
)


def test_get_array_from_any_1d_seq():
    for x in [
        np.ones((1, 3)),  # Vector "row"
        np.ones((3, 1)),  # Vector "column"
        pd.Series([1.0, 1.0, 1.0]),
        pd.DataFrame([1.0, 1.0, 1.0]),
    ]:
        res = get_array_from_any_1d_seq(x)
        assert np.array_equal(res, np.array([1.0, 1.0, 1.0]))


def test_calc_class_balance():
    res = calc_class_balance(np.array([0, 1, 1, 0, 0]))
    assert res == "60.0%:40.0%"


def test_calc_hash_for_seq():
    res = calc_hash_for_seq(np.array([1, 2, 3]))
    assert res == "1bcceb"
    res = calc_hash_for_seq(pd.Series([1, 2, 3]))
    assert res == "1bcceb"


def test_get_statistics_for_x_y__legend():
    res = get_statistics_for_x_y__legend()
    assert res == "X_shape; y_shape; hash;sum;balance"


def test_get_statistics_for_x_y():
    res = get_statistics_for_x_y(
        pd.DataFrame(np.ones((5, 3))), np.array([1, 0, 0, 1, 1])
    )
    assert res == "(5, 3); (5,); 1a1c92;3;40.0%:60.0%"


def test_data_frame_with_history():
    df_tmp = pd.DataFrame({"x": [1, 2, np.nan, 3], "y": [0, 1, 0, 0]})

    dfh_demo = DataFrameWithHistory(df_tmp)
    dfh_demo.modify_dataset(dfh_demo.df.dropna(), "NA values dropped")

    h = dfh_demo.history
    assert (
        h == "X_shape; y_shape; hash;sum;balance; comment\n"
        "(4, 2); ; ;;; Initial creation\n"
        "(3, 2); ; ;;; NA values dropped"
    )


def test_data_frame_pair_with_history():
    df_tmp = pd.DataFrame({"x": [1, 2, np.nan, 3], "y": [0, 1, 0, 0]})

    dfph_demo = DataFramePairWithHistory(df_tmp, df_tmp)
    dfph_demo.modify_dataset(
        dfph_demo.trn_df.dropna(), dfph_demo.tst_df, "Drop NAs for train"
    )

    h = dfph_demo.history
    assert (
        h == "Leg: X_shape; y_shape; hash;sum;balance; comment\n"
        "Trn: (4, 2); ; ;;; Initial creation\n"
        "Tst: (4, 2); ; ;;; Initial creation\n"
        "--\n"
        "Trn: (3, 2); ; ;;; Drop NAs for train\n"
        "Tst: (4, 2); ; ;;; Drop NAs for train\n"
        "--"
    )


def test_data_frame_pair_with_history__with_col_label():
    df_tmp = pd.DataFrame({"x": [1, 2, np.nan, 3], "y": [0, 1, 0, 0]})

    dfph_demo = DataFramePairWithHistory(
        df_tmp, df_tmp, col_label="y", initial_comment="abcd"
    )
    dfph_demo.modify_dataset(
        dfph_demo.trn_df.dropna(), dfph_demo.tst_df, "Drop NAs for train"
    )

    h = dfph_demo.history
    assert (
        h == "Leg: X_shape; y_shape; hash;sum;balance; comment\n"
        "Trn: (4, 1); (4,); 986649;1;75.0%:25.0%; abcd\n"
        "Tst: (4, 1); (4,); 986649;1;75.0%:25.0%; abcd\n"
        "--\n"
        "Trn: (3, 1); (3,); 907f06;1;66.7%:33.3%; Drop NAs for train\n"
        "Tst: (4, 1); (4,); 986649;1;75.0%:25.0%; Drop NAs for train\n"
        "--"
    )


def test_drop_some_rows_inplace():
    df_orig = pd.DataFrame({"x": [1, 2, 3, 4], "y": [0, 1, 0, 0]})

    df_tmp: pd.DataFrame = df_orig.copy()
    drop_some_rows_inplace(
        df_tmp, linked_df=None, n_final_rows=2, seed=42, verbose=True
    )
    assert len(df_tmp == 2)
    assert df_tmp.x.to_list() == [2, 3]

    # Another seed (results should be different)
    df_tmp = df_orig.copy()
    drop_some_rows_inplace(
        df_tmp, linked_df=None, n_final_rows=2, seed=43, verbose=True
    )
    assert len(df_tmp == 2)
    assert df_tmp.x.to_list() == [1, 4]

    # With linked df
    df_tmp = df_orig.copy()
    df_tmp2 = df_orig.copy()
    drop_some_rows_inplace(
        df_tmp, linked_df=df_tmp2, n_final_rows=2, seed=42, verbose=True
    )
    assert len(df_tmp == 2)
    assert df_tmp.x.to_list() == [2, 3]
    assert len(df_tmp2 == 2)
    assert df_tmp2.x.to_list() == [2, 3]
